class InvalidRequestError extends Error {
  constructor(message, statusCode = 400) {
    super(message);
    this.description = message;
    this.statusCode = statusCode;
  }
}

export default InvalidRequestError;
