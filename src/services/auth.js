import jwt from "jsonwebtoken";

import { ROLES } from "../constants/roles.js";

export async function authenticate(emailAddress) {
  const token = jwt.sign(
    {
      email: emailAddress,
      roles: [ROLES.WEATHER_SEEKER],
    },
    process.env.AUTH_SECRET_KEY
  );

  return {
    accessToken: token,
  };
}
