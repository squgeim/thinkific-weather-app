import axios from "axios";

import InvalidRequestError from "../utils/errors/InvalidRequest.js";

const WEATHER_APP_ID = process.env.WEATHER_APP_ID;

/**
 * Fetch current weather information for a city
 *
 * @param {string} cityName - query city
 */
export function fetchByCityName(cityName) {
  if (!WEATHER_APP_ID) {
    throw new Error("WEATHER_APP_ID not present in environment.");
  }

  return axios
    .get(`http://api.openweathermap.org/data/2.5/weather`, {
      params: {
        q: cityName,
        units: "metric",
        appid: WEATHER_APP_ID,
      },
    })
    .then((res) => res.data)
    .catch((err) => {
      throw new InvalidRequestError("Invalid city name provided.");
    });
}
