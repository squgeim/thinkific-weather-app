import { render } from "https://unpkg.com/lit-html?module";
import { getWeatherDetailHTML } from "./weather-detail.mjs";

let accessToken;

window.searchForm.addEventListener("submit", (e) => {
  e.preventDefault();

  const city = e.target.querySelector('[name="city"]')?.value;

  fetch(`/api/v1/weather/?city=${city}`, {
    headers: {
      Authorization: `bearer ${accessToken}`,
    },
  })
    .then((res) => res.json())
    .then((data) => {
      window.apiResponse.innerText = JSON.stringify(data, null, "    ");
      if (data.data) {
        render(getWeatherDetailHTML(data.data), window.weather);
      }
    })
    .catch((err) => {
      window.apiResponse.innerText = JSON.stringify(err, null, "    ");
    });
});

window.loginForm.addEventListener("submit", (e) => {
  e.preventDefault();

  const email = e.target.querySelector('[name="email"]')?.value;

  fetch(`/api/v1/auth/login`, {
    method: "POST",
    body: JSON.stringify({
      email,
    }),
    headers: {
      "Content-type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => data.data)
    .then((data) => {
      accessToken = data.accessToken;

      if (accessToken) {
        window.loginMsg.innerText = "Login successful";
      }
    })
    .catch((err) => {
      const p = document.createElement("p");
      p.innerText = JSON.stringify(err);
      window.loginMsg.appendChild(p);
    });
});
