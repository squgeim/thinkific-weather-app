import { html } from "https://unpkg.com/lit-html?module";

/**
 * @typedef {object} WeatherItem
 * @property {number} id
 * @property {string} main
 * @property {string} description
 * @property {string} icon
 */

/**
 * @typedef WeatherObject
 * @property {object} coord
 * @property {number} coord.lon
 * @property {number} coord.lat
 * @property {WeatherItem[]} weather
 * @property {string} base
 * @property {object} main
 * @property {number} main.temp
 * @property {number} main.feels_like
 * @property {number} main.temp_min
 * @property {number} main.temp_max
 * @property {number} main.pressure
 * @property {number} main.humidity
 * @property {number} visibility
 * @property {object} wind
 * @property {number} wind.speed
 * @property {number} wind.deg
 * @property {number} wind.gust
 * @property {object} clouds
 * @property {number} clouds.all
 * @property {number} dt
 * @property {object} sys
 * @property {number} sys.type
 * @property {number} sys.id
 * @property {string} sys.country
 * @property {number} sys.sunrise
 * @property {number} sys.sunset
 * @property {number} timezone
 * @property {number} id
 * @property {string} name
 * @property {number} cod
 */

/**
 * Returns an HTML element to display weather detail.
 *
 * @param {WeatherObject} weather - weather detail
 * @returns {HTMLElement}
 */
export function getWeatherDetailHTML(weather) {
  return html`
    <div
      style="display: flex; flex-direction: column; border: 1px solid black; align-items: center;"
    >
      <h1>${weather.main.temp}°C</h1>
      <div>Feels like <strong>${weather.main.feels_like}°C</strong></div>
      <span
        >${weather.weather[0].main} (${weather.weather[0].description})</span
      >
      <span>${weather.name}, ${weather.sys.country}</span>
    </div>
  `;
}
