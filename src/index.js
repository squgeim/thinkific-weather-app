import express from "express";
import path from "path";

import "dotenv/config";

import apiControllers from "./controllers/index.js";
import { globalErrorHandler } from "./middlewares/errorHandler.js";
import InvalidRequestError from "./utils/errors/InvalidRequest.js";

const PORT = process.env.PORT || 9000;

const app = express();

app.use(express.json());

app.use(
  "/api/:version/",
  (req, res, next) => {
    // Including version information in the param allows individual controllers
    // to handle api versions in the way that makes sense for them. Only a few
    // controllers may have distinction between various api versions.
    const { version } = req.params;

    if (!/v\d+/.test(version)) {
      return next(new InvalidRequestError("Invalid version number."));
    }

    return next();
  },
  apiControllers
);

const __dirname = path.dirname(new URL(import.meta.url).pathname);
app.use(express.static(path.join(__dirname, "public")));

// Global 404
app.use((req, res, next) => {
  res.sendStatus(404);
});

// Global error handler
app.use(globalErrorHandler);

app.listen(PORT, () => {
  console.log("Listening on port: ", PORT);
});
