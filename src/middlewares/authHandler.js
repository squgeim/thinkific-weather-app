import jwt from "jsonwebtoken";
import InvalidRequestError from "../utils/errors/InvalidRequest.js";

export function authenticate(allowRoles = []) {
  return (req, res, next) => {
    const { authorization } = req.headers;

    const [, accessToken] = (authorization ?? "").split("bearer ");

    try {
      const { email, roles } = jwt.verify(
        accessToken,
        process.env.AUTH_SECRET_KEY
      );

      req.user = { email };

      // Checking if the token has any roles and if those roles are valid for this route
      if (
        !roles.length ||
        !allowRoles.filter((r) => roles.includes(r)).length
      ) {
        throw new InvalidRequestError(
          "User is not authorized to perform this action",
          403
        );
      }
    } catch (err) {
      if (err instanceof InvalidRequestError) {
        return next(err);
      }

      if (err instanceof jwt.JsonWebTokenError) {
        return next(
          new InvalidRequestError(
            "Token is not valid. It is either malformed or has expired.",
            401
          )
        );
      }

      return next(err);
    }

    return next();
  };
}
