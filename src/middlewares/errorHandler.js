import InvalidRequestError from "../utils/errors/InvalidRequest.js";

function globalErrorHandler(err, req, res, next) {
  console.error(err);

  if (err instanceof InvalidRequestError) {
    return res.status(err.statusCode).json({
      error: err,
    });
  }

  res.status(500).json({
    error: err,
  });
}

export { globalErrorHandler };
