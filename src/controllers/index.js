import { Router } from "express";
import { ROLES } from "../constants/roles.js";
import { authenticate } from "../middlewares/authHandler.js";

import authControllers from "./auth.js";
import weatherControllers from "./weather.js";

const router = Router();

router.use("/auth", authControllers);
router.use(
  "/weather",
  authenticate([ROLES.WEATHER_SEEKER]),
  weatherControllers
);

export default router;
