import { Router } from "express";

import * as authService from "../services/auth.js";
import InvalidRequestError from "../utils/errors/InvalidRequest.js";

const router = new Router();

router.post("/login", (req, res, next) => {
  const { email } = req.body;

  if (!email) {
    return next(new InvalidRequestError("Email is required to log in."));
  }

  authService
    .authenticate(email)
    .then((data) => {
      res.json({ data });
    })
    .catch((err) => next(err));
});

export default router;
