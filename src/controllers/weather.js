import { Router } from "express";

import * as weatherServices from "../services/weather.js";
import InvalidRequestError from "../utils/errors/InvalidRequest.js";

const router = Router();

router.get("/", (req, res, next) => {
  const { city } = req.query;

  if (!city) {
    throw new InvalidRequestError("City is required.");
  }

  weatherServices
    .fetchByCityName(city)
    .then((weather) => {
      res.json({
        data: weather,
      });
    })
    .catch((err) => next(err));
});

export default router;
