### Date
2021-05-12

### Location of deployed application
https://sheltered-sands-47478.herokuapp.com/

### Time spent
~4 hours

### Assumptions made

- The assumption is that the API is just a proxy for the Open Weather Map API.

### Shortcuts/Compromises made

- This is a very simple implementation of the API designed to show the general project structure.
- There is very rudimentary validation of input. I would use some library to define schema for API
input (like Joi).
- You only need an email address to generate an access token. That's not much of an "authentication". :D
- JWT authentication only generates access tokens without an expiry date. A complete authentication
system would have both access tokens and refresh tokens, as well as mechanism to reissue tokens and
maintain a list of blacklisted tokens.
- The role-based access control implemented here is fine for very simple project, but in a larger
system you'd have to implement a more sophisticated access control system.
- The UI is built with plain JavaScript (ES2020). I choose not to implement any library like React because of
the limited time for this assignment.
- The weather service is pretty much a proxy for the Open Weather Map API so I have not defined my
own response payload.

### Stretch goals attempted

- A simple html UI is included. It does not use any SPA framework, it simply allows a user to log in,
 then input a city name. It only displays a simple UI and the raw API response.
- A simple JWT based authentication and role-based authorization system is included. It only issues
access tokens, does not issue a refresh token or any token revocation mechanism.
- App has been deployed on Heroku.
- The API acts as a proxy for Open Weather Map's current weather API.

### Instructions to run assignment locally

Install dependencies:

```sh
yarn install
```

Copy `.env.example` to `.env`. There are working env keys already present.

Then run the server. The server will listen at port `9000` by default. Use the environment variable
`PORT` to modify it.

```sh
yarn start
```

Use `yarn local` for development mode (restarts server upon change).

The frontend will be available at:
```sh
http://localhost:9000/
```

The API endpoints are:

```sh
http://localhost:9000/api/v1/weather
http://localhost:9000/api/v1/auth/login
```

### What did you not include in your solution that you want us to know about?

I think I have mentioned the corners I had to cut in the sections above.

### Other information about your submission that you feel it's important that we know if applicable.

N/A

### Your feedback on this technical challenge

I had fun doing the assignment. I hadn't had a chance to start a new express project from scratch in
some time. I took this opportunity to explore the recent development in ESM/ES2020.
